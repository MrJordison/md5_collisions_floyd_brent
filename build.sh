if [ ! -d "bin" ]; then
    mkdir bin;
fi
gcc src/hash_small.c src/util.c src/util.h src/md5_small.c -o bin/hash_small -Wall $1;
gcc src/floyd_1.c src/util.c src/util.h src/md5_small.c -o bin/floyd_1 -Wall $1;
gcc src/floyd_2.c src/util.c src/util.h src/md5_small.c -o bin/floyd_2 -Wall $1;
gcc src/floyd_3.c src/util.c src/util.h src/md5_small.c -o bin/floyd_3 -Wall $1;
gcc src/brent.c src/util.c src/util.h src/md5_small.c -o bin/brent -Wall $1;
gcc src/floyd_3_optimized.c src/util.c src/util.h src/md5_small.c -o bin/floyd_3_optimized -Wall $1;
gcc src/brent_optimized.c src/util.c src/util.h src/md5_small.c -o bin/brent_optimized -Wall $1;
