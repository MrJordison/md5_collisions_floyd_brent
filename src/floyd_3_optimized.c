//Local includes
#include "util.h"

//STD includes
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int main(int argc, char** argv)
{
    if(argc != 4)
    {
        fprintf(stderr, "Erreur, paramètre(s) manquant(s). Paramètre(s) désiré(s) : floyd_3 [image to collide] [image for attack] [taille en bit du hash]\n");
        exit(1);
    }

    printf("### Floyd 3 Optimized ###\n");

    //reading the two image files
    unsigned int bmpAttackSize, bmpToCollideSize;
    bmpAttackSize = bmpToCollideSize = 0;

    unsigned char* bmpAttack = ReadBmpContent(argv[1], &bmpAttackSize);
    unsigned char* bmpToCollide = ReadBmpContent(argv[2], &bmpToCollideSize);

    //Récupération de la taille de hash en bits et calcul du nombre d'octets nécessaire
    unsigned int hashBitSize = atoi(argv[3]);
    unsigned int hashByteLength = GetByteSize(hashBitSize);
    
    //déclaration d'un buffer utilisé pour afficher les différents hash en hexadécimal
    char* hexaHashStr = NULL;

    if(bmpAttackSize < hashBitSize)
    {
        fprintf(stderr, "Erreur, la taille de l'image %s est inférieure au nombre de bits.\n", argv[1]);
        exit(1);
    }
    if(bmpToCollideSize < hashBitSize)
    {
        fprintf(stderr, "Erreur, la taille de l'image %s est inférieure au nombre de bits.\n", argv[2]);
        exit(1);
    }

    //instanciation des hash
    unsigned char x[hashByteLength];
    unsigned char y[hashByteLength];

    //Découpe des images en deux parties : la fin de l'image pour injecter hashKey
    // et le debut qui ne change pour générer le début de md5 pour chacune 

    unsigned char beginAttackImage[bmpAttackSize - hashBitSize];
    unsigned char endAttackImage[hashBitSize];
    unsigned char beginToCollideImage[bmpToCollideSize - hashBitSize];
    unsigned char endToCollideImage[hashBitSize];
    memcpy(beginAttackImage, bmpAttack, bmpAttackSize - hashBitSize);
    memcpy(beginToCollideImage, bmpToCollide, bmpToCollideSize - hashBitSize);
    memcpy(endAttackImage, bmpAttack + (bmpAttackSize - hashBitSize), hashBitSize);
    memcpy(endToCollideImage, bmpToCollide + (bmpToCollideSize - hashBitSize), hashBitSize);
    
    //struct pour générer le md5 de chacune des images, en passant le début de chacune qui est fixe
    MD5_CTX md5Attack;
    MD5Init(&md5Attack);
    MD5Update(&md5Attack, beginAttackImage, bmpAttackSize - hashBitSize);

    MD5_CTX md5ToCollide;
    MD5Init(&md5ToCollide);
    MD5Update(&md5ToCollide, beginToCollideImage, bmpToCollideSize - hashBitSize);

    
    printf("Début de la passe 1.\n");
    unsigned int i = 0;
    
    unsigned char x_image[hashBitSize];
    unsigned char y_image[hashBitSize];
    MD5_CTX x_md5;
    MD5_CTX y_md5;

    memcpy(x_image, endAttackImage, hashBitSize);
    memcpy(y_image, endAttackImage, hashBitSize);
    x_md5 = md5Attack;
    y_md5 = md5Attack;

    //démarre le début du timer
    clock_t startTimer = clock();

    do
    {
        if(i != 0)
        {
            if(switchImage(x))
            {
                memcpy(x_image, endAttackImage, hashBitSize);
                x_md5 = md5Attack;
            }
            else
            {
                memcpy(x_image, endToCollideImage, hashBitSize);
                x_md5 = md5ToCollide;
            }
            if(switchImage(y))
            {
                memcpy(y_image, endAttackImage, hashBitSize);
                y_md5 = md5Attack;
            }
            else
            {
                memcpy(y_image, endToCollideImage, hashBitSize);
                y_md5 = md5ToCollide;
            }

            f_function(x_image, hashBitSize, x, hashByteLength);
            f_function(y_image, hashBitSize, y, hashByteLength);
        }

        //hash de x et ecrit dans une nouvelle version de l'image
        md5_small_optimized(x_md5, x, hashByteLength, hashBitSize, x_image, hashBitSize);

        //hash de y
        md5_small_optimized(y_md5, y, hashByteLength, hashBitSize, y_image, hashBitSize);
        if(switchImage(y))
        {
            memcpy(y_image, endAttackImage, hashBitSize);
            y_md5 = md5Attack;
        }
        else
        {
            memcpy(y_image, endToCollideImage, hashBitSize);
            y_md5 = md5ToCollide;
        }
        f_function(y_image, hashBitSize, y, hashByteLength);
        md5_small_optimized(y_md5, y, hashByteLength, hashBitSize, y_image, hashBitSize);

        //Display
        if(i != 0 && i%100 == 0)
            ReleasePrintf("%u tours effectués...\n", i);
        DebugPrintf("Tour %u: x = %s y = %s\n", 
            i + 1, 
                    PrintHexaUnsignedCharArray(x, hashByteLength, hexaHashStr),
                    PrintHexaUnsignedCharArray(y, hashByteLength, hexaHashStr));

        ++i;
    }
    while(memcmp(x, y, hashByteLength * sizeof(unsigned char)) != 0);

    unsigned int i1 = i;
    
    printf("Début de la passe 2.\n");

    if(switchImage(x))
    {
        memcpy(y_image, endAttackImage, hashBitSize);
        y_md5 = md5Attack;
    }
    else
    {
        memcpy(y_image, endToCollideImage, hashBitSize);
        y_md5 = md5ToCollide;
    }
    
    f_function(y_image, hashBitSize, x, hashByteLength);
    memcpy(x_image, endAttackImage, hashBitSize);
    x_md5 = md5Attack;

    i = 0;
    do
    {
        if(i != 0)
        {
            if(switchImage(x))
            {
                memcpy(x_image, endAttackImage, hashBitSize);
                x_md5 = md5Attack;
            }
            else
            {
                memcpy(x_image, endToCollideImage, hashBitSize);
                x_md5 = md5ToCollide;
            }
            f_function(x_image, hashBitSize, x, hashByteLength);
            if(switchImage(y))
            {
                memcpy(y_image, endAttackImage, hashBitSize);
                y_md5 = md5Attack;
            }
            else
            {
                memcpy(y_image, endToCollideImage, hashBitSize);
                y_md5 = md5ToCollide;
            }
            f_function(y_image, hashBitSize, y, hashByteLength);
        }

        //hash de x
        md5_small_optimized(x_md5, x, hashByteLength, hashBitSize, x_image, hashBitSize);

        //hash de y
        md5_small_optimized(y_md5, y, hashByteLength, hashBitSize, y_image, hashBitSize);

        //Display
        if(i != 0 && i%100 == 0)
            ReleasePrintf("%u tours effectués...\n", i);
        DebugPrintf("Tour %u: x = %s y = %s\n", 
            i + 1, 
                    PrintHexaUnsignedCharArray(x, hashByteLength, hexaHashStr),
                    PrintHexaUnsignedCharArray(y, hashByteLength, hexaHashStr));

        ++i;
    }
    while(memcmp(x, y, hashByteLength * sizeof(unsigned char)) != 0);
    
    unsigned int i2 = i;

    clock_t endTimer = clock();
    double execTime = (double)(startTimer = endTimer) / CLOCKS_PER_SEC;

    printf("Nombre de tours pour la passe 1 : %u (2^%u)\nNombre de tours pour la passe 2 : %u (2^%u)\nTemps d'exécution : %f seconde(s)\n", i1, GetPow2(i1), i2, GetPow2(i2), execTime);
    printf("Collision trouvée avec la valeur de clé égale à : %s\n", PrintHexaUnsignedCharArray(x,hashByteLength, hexaHashStr));

    //Clean des allocations
    free(bmpAttack);
    free(bmpToCollide);
    if(hexaHashStr != NULL)
        free(hexaHashStr);
    
    return 0;
}