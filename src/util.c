//Local includes
#include "util.h"

//STD includes
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* PrintHexaUnsignedCharArray(unsigned char* array, unsigned int length, char* result)
{
    if(result != NULL)
        free(result);
    result = (char*) malloc((length * 2 + 1) * sizeof(char));
    char buffer [3];
    for(unsigned int i = 0; i < length; ++i)
    {
        sprintf(buffer, "%02x", array[i]);
        result[i*2] = buffer[0];
        result[i*2 + 1] = buffer[1];
    }
    result[length *2] = '\0';
    return result;
}

unsigned int GetByteSize(unsigned hashBitSize)
{
    if(hashBitSize <= 0 || hashBitSize > 128)
    {
        fprintf(stderr, "Erreur, le nombre de bits %u n'est pas correct, il doit être compris entre 1 et 128", hashBitSize);
        exit(1);
    }
    unsigned int hashByteLength = hashBitSize / 8;
    if(hashBitSize % 8 != 0)
        hashByteLength += 1;

    return hashByteLength;
}

unsigned int GetPow2(unsigned int i)
{
    unsigned int temp = i;
    unsigned int result = 0;
    while(temp > 1)
    {
        temp = temp >> 1;
        ++result;
    }

    return result;
}

unsigned char* ReadFile(char* filename, unsigned int * contentSize)
{
    FILE* file;
    unsigned char* content;

    //open file to read in binary mode
    file = fopen(filename, "rb");

    if(file == NULL)
    {
        printf("Erreur, le fichier spécifié %s est introuvable.", filename);
        exit(1);
    }

    //get content file size
    fseek(file, 0, SEEK_END); //put file cursor to end of file
    *contentSize = ftell(file); //file cursor position gives number of bytes in file
    rewind(file); //reinit file cursor position

    //read file content
    content = (unsigned char*) malloc(*contentSize * sizeof(unsigned char));
    fread(content, sizeof(unsigned char), *contentSize, file);

    //closing file after reading
    fclose(file);

    return content;
}

unsigned char* ReadBmpContent(char* filename, unsigned int* bmpContentSize)
{
    //Vérifie que le fichier ait la bonne extension bmp
    char * splitFileName = strrchr(filename, '.');
    if(!splitFileName)
    {
        fprintf(stderr, "Erreur, le fichier %s n'a pas d'extension.\n", filename);
        exit(1);
    }
    else if(strcmp(splitFileName + 1, "bmp") != 0)
    {
        fprintf(stderr, "Erreur, le fichier %s n'est pas un fichier bitmap (l'extension n'est pas .bmp).\n", filename);
        exit(1);
    }

    //Récupération du contenu du fichier
    unsigned char* content;
    unsigned int contentSize = 0;
    content = ReadFile(filename, &contentSize);

    //Récupération de la position de départ du contenu de l'image, dont la valeur est stockée à l'octet 10 du header
    //du fichier bmp
    unsigned int bmpDataOffset = 0;
    memcpy(&bmpDataOffset, content + 10, 4);

    //calcul de la taille du contenu de l'image (sans le header)
    *bmpContentSize = contentSize - bmpDataOffset;

    //copie du contenu de l'image et retourne cette dernière
    unsigned char* bmpContent = (unsigned char*) malloc(*bmpContentSize * sizeof(unsigned char));
    DebugPrintf("Taille du contenu de %s (en octets) : %u et en pixels : %u\n", filename, *bmpContentSize, *bmpContentSize / 4);
    memcpy(bmpContent, content + bmpDataOffset, *bmpContentSize);

    //Clean des allocations
    free(content);

    return bmpContent;
}

void f_function(unsigned char* image, unsigned int image_size, unsigned char* hash, unsigned int hash_size)
{
    if(hash_size * 8 > image_size)
    {
        printf("Erreur, la taille de l'image n'est pas suffisante pour y stocker le hash dans les bits de poids faible.");
        exit(1);
    }

    unsigned int cursor = 0;

    //pour chaque octet du hash
    for(unsigned int i = 0 ; i < hash_size ; ++i)
    {
        //pour chaque bit de cet octet
        for(unsigned char x = 128; x > 0 ; x >>=1)
        {
            //mettre le bit de poids faible de l'octet à l'indice cursor dans l'image à 1 ou 0 selon le bit courant
            if(x & hash[i])
                image[cursor] = image[cursor] | 1;

            else
                image[cursor] = image[cursor] & 254;
            cursor++;
        }
    }
}

bool switchImage(unsigned char* hash)
{
    //si la différence du premier caractère et de 128 est supérieure à 0, alors le premier bit est égal à 1
    if(hash[0]-'\x10' > 0)
        return true;
    return false;
}

int md5_small_optimized(MD5_CTX mdContext, unsigned char* hash, unsigned int hash_byte_length, unsigned int hash_bit_size, unsigned char* input, unsigned int input_byte_length)
{
  int i;

  if (hash_bit_size > 128)
  {
    fprintf(stderr, "ERROR: hash_bit_size must not be more than 128\n");
    return -1;
  }
  if (hash_byte_length > 16)
  {
    fprintf(stderr, "ERROR: hash_byte_length must not be more than 16\n");
    return -1;
  }
  if (hash_byte_length != ((hash_bit_size - 1) / 8) + 1)
  {
    fprintf(stderr, "ERROR: hash_byte_length is not consistent with hash_bit_size\n");
    return -1;
  }
 
  MD5Update (&mdContext, input, input_byte_length);
  MD5Final (&mdContext);

  i = 0;
  while (hash_bit_size > 0)
  {
    if (hash_bit_size >= 8)
    {
      hash[(hash_byte_length - 1) - i] = mdContext.digest[15 - i];
      hash_bit_size -= 8;
    }
    else
    {
      hash[(hash_byte_length - 1) - i] = mdContext.digest[15 - i] & (0xFF >> (8 - hash_bit_size));
      hash_bit_size = 0;
    }
    i++;
  }
  
  return 0;
}