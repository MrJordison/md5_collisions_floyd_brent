#ifndef util_h
#define util_h

//Local includes
#include "md5_small.h"

//STD includes
#include <stdbool.h>

#ifdef DEBUG 
#define DebugPrintf(outStr, ...) printf(outStr, ##__VA_ARGS__);
#else
#define DebugPrintf(outStr, ...) ;
#endif

#ifdef RELEASE
#define ReleasePrintf(outStr, ...) printf(outStr, ##__VA_ARGS__);
#else
#define ReleasePrintf(outStr, ...) ;
#endif

//retourne en chaine de caractère la forme hexa du tableau d'unsigned char passé en paramètre
//le second paramètre est la taille totale de la chaine de caractères retournée
//le troisième est le pointeur dans lequel sera stocké l'adresse mémoire de la chaine créée et retournée, si il est non nul lors de l'appel de la fonction, son contenu sera libéré avant la création de la chaine
char* PrintHexaUnsignedCharArray(unsigned char* array, unsigned int length, char* result);

//permet de calculer le nombre d'octets nécessaire à un haché dont la taille est de hashBitSize bits
//hashBitSize doit être compris entre 1 et 128 inclus
unsigned int GetByteSize(unsigned hashBitSize);

unsigned int GetPow2(unsigned int i);

//Lit un fichier dont le path est donné en paramètre et retourne le contenu dans un unsigned char* alloué sur le tas
//dont la taille est stockée dans contentSize
unsigned char* ReadFile(char* filename, unsigned int* contentSize);

//Cette fonction lit un fichier bitmap et retourne un pointeur vers un tableau d'unsigned char * alloué sur le tas,
// contenant uniquement le contenu de l'image. La taille du tableau de retour est stockée dans bmpContentSize 
unsigned char* ReadBmpContent(char* filename, unsigned int* bmpContentSize);

//But de la fonction: écrire via la steganographie
//ecrit le hash dans le content de l'image dans les bits de poid faible
//utilise le décalage de bit pour l'écriture du bit de poids faible
void f_function(unsigned char* image, unsigned int image_size, unsigned char* hash, unsigned int hash_size);

//Fonction qui retourne un booléen selon le premier bit du hash passé en paramètre:
//Si le bit == 1, return vrai, sinon retourne faux
bool switchImage(unsigned char* hash);

int md5_small_optimized(MD5_CTX mdContext, unsigned char* hash, unsigned int hash_byte_length, unsigned int hash_bit_size, unsigned char* input, unsigned int input_byte_length);

#endif //util_h