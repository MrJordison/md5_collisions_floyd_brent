//Local includes
#include "util.h"

//STD includes
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int main(int argc, char** argv)
{
    if(argc != 3)
    {
        fprintf(stderr, "Erreur, paramètre(s) manquant(s). Paramètre(s) désiré(s) : floyd_2 [nom du fichier bitmap] [taille en bit du hash]\n");
        exit(1);
    }

    printf("### Floyd 2 ###\n");

    unsigned char* bmpContent;
    unsigned int bmpContentSize = 0;

    bmpContent = ReadBmpContent(argv[1], &bmpContentSize);

    //Récupération de la taille de hash en bits et calcul du nombre d'octets nécessaire
    unsigned int hashBitSize = atoi(argv[2]);
    unsigned int hashByteLength = GetByteSize(hashBitSize);
    
    //déclaration d'un buffer utilisé pour afficher les différents hash en hexadécimal
    char* hexaHashStr = NULL;

    if(bmpContentSize < hashBitSize)
    {
        fprintf(stderr, "Erreur, la taille de l'image %s est inférieure au nombre de bits.\n", argv[1]);
        exit(1);
    }

    //instanciation des hash
    unsigned char x[hashByteLength];
    unsigned char y[hashByteLength];

    printf("Début de la passe 1.\n");
    unsigned int i = 0;

    unsigned char x_image[bmpContentSize];
    memcpy(x_image, bmpContent, bmpContentSize);
    unsigned char y_image[bmpContentSize];
    memcpy(y_image, bmpContent, bmpContentSize);

    //démarre le début du timer
    clock_t startTimer = clock();

    do
    {
        //hash de x et ecrit dans une nouvelle version de l'image
        md5_small(x, hashByteLength, hashBitSize, x_image, bmpContentSize);
        f_function(x_image, bmpContentSize, x, hashByteLength);

        //hash de y
        md5_small(y, hashByteLength, hashBitSize, y_image, bmpContentSize);
        f_function(y_image, bmpContentSize, y, hashByteLength);
        md5_small(y, hashByteLength, hashBitSize, y_image, bmpContentSize);
        f_function(y_image, bmpContentSize, y, hashByteLength);

        //Display
        if(i != 0 && i%100 == 0)
            ReleasePrintf("%u tours effectués...\n", i);
        DebugPrintf("Tour %u: x = %s y = %s\n", 
            i + 1, 
                    PrintHexaUnsignedCharArray(x, hashByteLength, hexaHashStr),
                    PrintHexaUnsignedCharArray(y, hashByteLength, hexaHashStr));

        ++i;
    }
    while(memcmp(x, y, hashByteLength * sizeof(unsigned char)) != 0);

    unsigned int i1 = i;
    
    printf("Début de la passe 2.\n");

    memcpy(y_image, x_image, bmpContentSize * sizeof(unsigned char));
    memcpy(x_image, bmpContent, bmpContentSize * sizeof(unsigned char));

    i = 0;
    do
    {
        //hash de x
        md5_small(x, hashByteLength, hashBitSize, x_image, bmpContentSize);
        f_function(x_image, bmpContentSize, x, hashByteLength);

        //hash de y
        md5_small(y, hashByteLength, hashBitSize, y_image, bmpContentSize);
        f_function(y_image, bmpContentSize, y, hashByteLength);

        //Display
        if(i != 0 && i%100 == 0)
            ReleasePrintf("%u tours effectués...\n", i);
        DebugPrintf("Tour %u: x = %s y = %s\n", 
            i + 1, 
                    PrintHexaUnsignedCharArray(x, hashByteLength, hexaHashStr),
                    PrintHexaUnsignedCharArray(y, hashByteLength, hexaHashStr));

        ++i;
    }
    while(memcmp(x, y, hashByteLength * sizeof(unsigned char)) != 0);

    unsigned int i2 = i;
    
    clock_t endTimer = clock();
    double execTime = (double)(startTimer = endTimer) / CLOCKS_PER_SEC;

    printf("Nombre de tours pour la  passe 1 : %u (2^%u)\nNombre de tours pour la passe 2 : %u (2^%u)\nTemps d'exécution : %f seconde(s)\n", i1, GetPow2(i1), i2, GetPow2(i2), execTime);
    printf("Collision trouvée avec la valeur de clé égale à : %s\n", PrintHexaUnsignedCharArray(x,hashByteLength, hexaHashStr));
 
    //Clean des allocations
    free(bmpContent);
    if(hexaHashStr != NULL)
        free(hexaHashStr);

    return 0;
}