//Local includes
#include "util.h"

//STD includes
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int main(int argc, char** argv)
{
    if(argc != 4)
    {
        fprintf(stderr, "Erreur, paramètre(s) manquant(s). Paramètre(s) désiré(s) : brent [image to collide] [image for attack] [taille en bit du hash]\n");
        return -1;
    }

    printf("### Brent ###\n");

    //reading the two image files
    unsigned int bmpAttackSize, bmpToCollideSize;
    bmpAttackSize = bmpToCollideSize = 0;

    unsigned char* bmpAttack = ReadBmpContent(argv[1], &bmpAttackSize);
    unsigned char* bmpToCollide = ReadBmpContent(argv[2], &bmpToCollideSize);

    //Récupération de la taille de hash en bits et calcul du nombre d'octets nécessaire
    unsigned int hashBitSize = atoi(argv[3]);
    unsigned int hashByteLength = GetByteSize(hashBitSize);
    
    //déclaration d'un buffer utilisé pour afficher les différents hash en hexadécimal
    char* hexaHashStr = NULL;

    if(bmpAttackSize < hashBitSize)
    {
        fprintf(stderr, "Erreur, la taille de l'image %s est inférieure au nombre de bits.\n", argv[1]);
        exit(1);
    }
    if(bmpToCollideSize < hashBitSize)
    {
        fprintf(stderr, "Erreur, la taille de l'image %s est inférieure au nombre de bits.\n", argv[2]);
        exit(1);
    }

    //instanciation des hash
    unsigned char x[hashByteLength];
    unsigned char y[hashByteLength];

    printf("Début de la passe 1.\n");
    unsigned int i = 0;
    
    unsigned char* x_image;
    unsigned char* y_image;
    unsigned int xImageSize = bmpAttackSize;
    unsigned int yImageSize = bmpAttackSize;

    if(bmpAttackSize > bmpToCollideSize)
    {
        x_image = (unsigned char*) malloc(bmpAttackSize * sizeof(unsigned char));
        y_image = (unsigned char*) malloc(bmpAttackSize * sizeof(unsigned char));
    }
    else
    {
        x_image = (unsigned char*) malloc(bmpToCollideSize * sizeof(unsigned char));
        y_image = (unsigned char*) malloc(bmpToCollideSize * sizeof(unsigned char));

    }

    unsigned int pow, cpt_pow;
    pow = 1;
    cpt_pow = 1;
    memcpy(y_image, bmpAttack, yImageSize);

    //démarre le début du timer
    clock_t startTimer = clock();
    
    md5_small(y, hashByteLength, hashBitSize, y_image, yImageSize);
            if(switchImage(y))
						{
                yImageSize = bmpAttackSize;
                memcpy(y_image, bmpAttack, bmpAttackSize);
						}
						else
						{
							yImageSize = bmpToCollideSize;
							memcpy(y_image, bmpToCollide, bmpToCollideSize);
						}
    f_function(y_image, yImageSize, y, hashByteLength);


    do
    {
        if(i != 0)
        {
					if(switchImage(y))
					{
						yImageSize = bmpAttackSize;
						memcpy(y_image, bmpAttack, bmpAttackSize);
					}
					else
					{
						yImageSize = bmpToCollideSize;
						memcpy(y_image, bmpToCollide, bmpToCollideSize);
					}
					f_function(y_image, yImageSize, y, hashByteLength);
        }

        if(cpt_pow == pow)
        {
            //Affichage
            DebugPrintf("%u est une puissance de 2, avancement de x jusqu'à y = %s\n", cpt_pow, PrintHexaUnsignedCharArray(y, hashByteLength, hexaHashStr));

            cpt_pow = 0;
            pow = pow << 1;
            memcpy(x, y, hashByteLength);
        }
        ++cpt_pow;

        //hash de y
				md5_small(y, hashByteLength, hashBitSize, y_image, yImageSize);

        //Display
        if(i != 0 && i%100 == 0)
            ReleasePrintf("%u tours effectués...\n", i);
        
        DebugPrintf("Tour %u: x = %s y = %s\n", 
            i + 1, 
                    PrintHexaUnsignedCharArray(x, hashByteLength, hexaHashStr),
                    PrintHexaUnsignedCharArray(y, hashByteLength, hexaHashStr));

        ++i;
    }
    while(memcmp(x, y, hashByteLength * sizeof(unsigned char)) != 0);

    unsigned int i1 = i;
    
    printf("Début de la passe 2.\n");

    yImageSize = bmpAttackSize;
    memcpy(y_image, bmpAttack, yImageSize);
	
    unsigned int i2y;
    for(i2y = 0; i2y < cpt_pow; ++i2y)
    {
				md5_small(y, hashByteLength, hashBitSize, y_image, yImageSize);
				if(switchImage(y))
				{
					yImageSize = bmpAttackSize;
					memcpy(y_image, bmpAttack, bmpAttackSize);
				}
				else
				{
					yImageSize = bmpToCollideSize;
					memcpy(y_image, bmpToCollide, bmpToCollideSize);
				}
				f_function(y_image, yImageSize, y, hashByteLength);

        //Display
        if(i2y != 0 && i2y%100 == 0)
            ReleasePrintf("%u tours effectués pour y...\n", i2y);
        DebugPrintf("Tour %u: y = %s\n", 
            i2y + 1, 
                    PrintHexaUnsignedCharArray(y, hashByteLength, hexaHashStr));

    }

    unsigned int i2x = 0;

    xImageSize = bmpAttackSize;
    memcpy(x_image, bmpAttack, xImageSize);
    do
    {
        md5_small(x, hashByteLength, hashBitSize, x_image, yImageSize);
        md5_small(y, hashByteLength, hashBitSize, y_image, yImageSize);
        if(switchImage(x))
        {
            xImageSize = bmpAttackSize;
            memcpy(x_image, bmpAttack, xImageSize);
        }
        else
        {
            xImageSize = bmpToCollideSize;
            memcpy(x_image, bmpToCollide, xImageSize);
        }
        if(switchImage(y))
        {
            yImageSize = bmpAttackSize;
            memcpy(y_image, bmpAttack, yImageSize);
        }
        else
        {
            yImageSize = bmpToCollideSize;
            memcpy(y_image, bmpToCollide, yImageSize);
        }
        f_function(x_image, xImageSize, x, hashByteLength);
        f_function(y_image, yImageSize, y, hashByteLength);

        //Display
        if(i2x != 0 && i2x%100 == 0)
            ReleasePrintf("%u tours effectués pour x et %u tours pour y...\n", i2x, i2y);
        DebugPrintf("Tour %u pour x = %s\nTour %u pour y = %s\n", 
            i2x + 1, 
            PrintHexaUnsignedCharArray(x, hashByteLength, hexaHashStr),
            i2y + 1, 
            PrintHexaUnsignedCharArray(y, hashByteLength, hexaHashStr));

        ++i2x;
        ++i2y;
    }
    while(memcmp(x, y, hashByteLength * sizeof(unsigned char)) != 0);

    clock_t endTimer = clock();
    double execTime = (double)(startTimer = endTimer) / CLOCKS_PER_SEC;

    printf("Nombre de tours pour la passe 1 : %u (2^%u)\n", i1, GetPow2(i1));
    printf("Nombre de tours pour la passe 2 pour y : %u (2^%u) et nombre de tours pour x : %u (2^%u)\n", i2y, GetPow2(i2y), i2x, GetPow2(i2x));
    printf("Temps d'exécution : %f seconde(s)\n", execTime);
    printf("Collision trouvée avec la valeur de clé égale à : %s\n", PrintHexaUnsignedCharArray(x, hashByteLength, hexaHashStr));
    
    //Clean des allocations
    free(bmpAttack);
    free(bmpToCollide);
    if(hexaHashStr != NULL)
        free(hexaHashStr);

    return 0;

}