//Local includes
#include "util.h"

//STD includes
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int main(int argc, char** argv)
{
    if(argc != 2)
    {
        fprintf(stderr, "Erreur, paramètre(s) manquant(s). Paramètre(s) désiré(s) : floyd_1 [taille en bits du hash]");
        exit(1);
    }

    printf("### Floyd 1 ###\n");

    //Récupération de la taille de hash en bits et calcul du nombre d'octets nécessaire
    unsigned int hashBitSize = atoi(argv[1]);
    unsigned int hashByteLength = GetByteSize(hashBitSize);

    //déclaration d'un buffer utilisé pour afficher les différents hash en hexadécimal
    char* hexaHashStr = NULL;

    //création d'un hash initial
    unsigned char initialHash[hashByteLength];
    srand(time(NULL));
    for(unsigned int i = 0; i < hashByteLength; ++i)
        initialHash[i] = rand();

    printf("Hash initial généré: %s\n", PrintHexaUnsignedCharArray(initialHash, hashByteLength, hexaHashStr));

    //instanciation des buffers de hash
    unsigned char x[hashByteLength];
    unsigned char y[hashByteLength];
    
    //copie le hash initial dans les buffers
    memcpy(x, initialHash, hashByteLength * sizeof(unsigned char));
    memcpy(y, initialHash, hashByteLength * sizeof(unsigned char));

    printf("Début de la passe 1.\n");

    unsigned int i = 0;

    //démarre le début du timer
    clock_t startTimer = clock();

    do
    {
        //hash de x
        md5_small(x, hashByteLength, hashBitSize, x, hashByteLength);

        //hash de y
        md5_small(y, hashByteLength, hashBitSize, y, hashByteLength);
        md5_small(y, hashByteLength, hashBitSize, y, hashByteLength);


        //Display
        if(i != 0 && i%100 == 0)
            ReleasePrintf("%u tours effectués...\n", i);
        DebugPrintf("Tour %u: x = %s y = %s\n", 
            i + 1, 
                    PrintHexaUnsignedCharArray(x, hashByteLength, hexaHashStr),
                    PrintHexaUnsignedCharArray(y, hashByteLength, hexaHashStr));


        ++i;
    }
    while(memcmp(x, y, hashByteLength * sizeof(unsigned char)) != 0);

    unsigned int i1 = i;
    
    printf("Début de la passe 2.\n");

    //Copie du hash x en y et réinitialisation de y
    memcpy(y, x, hashByteLength * sizeof(unsigned char));
    memcpy(x, initialHash, hashByteLength * sizeof(unsigned char));

    i = 0;
    do
    {
        //hash de x
        md5_small(x, hashByteLength, hashBitSize, x, hashByteLength);

        //hash de y
        md5_small(y, hashByteLength, hashBitSize, y, hashByteLength);

        //Display
        if(i != 0 && i%100 == 0)
            ReleasePrintf("%u tours effectués...\n", i);
        DebugPrintf("Tour %u: x = %s y = %s\n", 
            i + 1, 
                    PrintHexaUnsignedCharArray(x, hashByteLength, hexaHashStr),
                    PrintHexaUnsignedCharArray(y, hashByteLength, hexaHashStr));


        ++i;
    }
    while(memcmp(x, y, hashByteLength * sizeof(unsigned char)) != 0);
    
    unsigned int i2 = i;

    clock_t endTimer = clock();
    double execTime = (double)(startTimer = endTimer) / CLOCKS_PER_SEC;

    printf("Nombre de tours pour la passe 1 : %u (2^%u)\nNombre de tours pour la passe 2 : %u (2^%u)\nTemps d'exécution : %f seconde(s)\n", i1, GetPow2(i1), i2, GetPow2(i2), execTime);
    printf("Collision trouvée avec la valeur de clé égale à : %s\n", PrintHexaUnsignedCharArray(x,hashByteLength, hexaHashStr));
    
    if(hexaHashStr != NULL)
        free(hexaHashStr);

    return 0;
}