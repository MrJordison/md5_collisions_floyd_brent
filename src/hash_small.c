//Local includes
#include "md5_small.h"

//STD includes
#include <stdio.h>
#include <stdlib.h>
#include "util.h"

int main(int argc, char** argv)
{
    //Vérification des paramètres
    if(argc != 3)
    {
        fprintf(stderr, "Paramètre(s) manquant(s). Paramètre(s) désiré(s) : hash_small [nom du fichier] [taille en bit du hash]");
        exit(1);
    }
    char* filename = argv[1];
    
    //Récupération de la taille de hash en bits et calcul du nombre d'octets nécessaire
    unsigned int hashBitSize = atoi(argv[2]);
    unsigned int hashByteLength = GetByteSize(hashBitSize);

    printf("Taille du haché en bits : %u\nFichier à hacher: %s\n", hashBitSize, filename);

    //déclaration du buffer de hash
    unsigned char hash[hashByteLength];

    printf("Lecture du fichier contenant l'exemple de wikipédia.\n");
    unsigned int contentSize;
    unsigned char* content = ReadFile(filename, &contentSize);

    //déclaration d'un buffer utilisé pour afficher les différents hash en hexadécimal
    char* hexaHashStr = NULL;   
    
    //hash du contenu du fichier
    md5_small(hash, hashByteLength, hashBitSize, content, contentSize);
    
    printf("Valeur de hash du contenu : %s\n", PrintHexaUnsignedCharArray(hash, hashByteLength, hexaHashStr));

    //Clean des allocations
    free(content);
    if(hexaHashStr != NULL)
        free(hexaHashStr);
    
    return 0;
}
