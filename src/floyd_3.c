//Local includes
#include "util.h"

//STD includes
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int main(int argc, char** argv)
{
    if(argc != 4)
    {
        fprintf(stderr, "Erreur, paramètre(s) manquant(s). Paramètre(s) désiré(s) : floyd_3 [image to collide] [image for attack] [taille en bit du hash]\n");
        return -1;
    }

    printf("### Floyd 3 ###\n");

    //reading the two image files
    unsigned int bmpAttackSize, bmpToCollideSize;
    bmpAttackSize = bmpToCollideSize = 0;

    unsigned char* bmpAttack = ReadBmpContent(argv[1], &bmpAttackSize);
    unsigned char* bmpToCollide = ReadBmpContent(argv[2], &bmpToCollideSize);
    
    //Récupération de la taille de hash en bits et calcul du nombre d'octets nécessaire
    unsigned int hashBitSize = atoi(argv[3]);
    unsigned int hashByteLength = GetByteSize(hashBitSize);
    
    //déclaration d'un buffer utilisé pour afficher les différents hash en hexadécimal
    char* hexaHashStr = NULL;

    if(bmpAttackSize < hashBitSize)
    {
        fprintf(stderr, "Erreur, la taille de l'image %s est inférieure au nombre de bits.\n", argv[1]);
        exit(1);
    }
    if(bmpToCollideSize < hashBitSize)
    {
        fprintf(stderr, "Erreur, la taille de l'image %s est inférieure au nombre de bits.\n", argv[2]);
        exit(1);
    }

    //instanciation des hash
    unsigned char x[hashByteLength];
    unsigned char y[hashByteLength];

    printf("Début de la passe 1.\n");
    unsigned int i = 0;

    unsigned int xImageSize = bmpAttackSize;
    unsigned int yImageSize = bmpAttackSize;
	
		unsigned char * x_image;
		unsigned char * y_image;
		if(bmpAttackSize > bmpToCollideSize)
		{
			x_image = (unsigned char*) malloc(bmpAttackSize * sizeof(unsigned char));
			y_image = (unsigned char*) malloc(bmpAttackSize * sizeof(unsigned char));
		}
		else
		{
			x_image = (unsigned char*) malloc(bmpToCollideSize * sizeof(unsigned char));
			y_image = (unsigned char*) malloc(bmpToCollideSize * sizeof(unsigned char));

		}
    
    memcpy(x_image, bmpAttack, bmpAttackSize);
    memcpy(y_image, bmpAttack, bmpAttackSize);

    //démarre le début du timer
    clock_t startTimer = clock();

    do
    {
        if(i != 0)
        {
            if(switchImage(x))
            {
                xImageSize = bmpAttackSize;
                memcpy(x_image, bmpAttack, bmpAttackSize);
            }
            else
            {
                xImageSize = bmpToCollideSize;
                memcpy(x_image, bmpToCollide, bmpToCollideSize);
            }
            if(switchImage(y))
            {
                yImageSize = bmpAttackSize;
                memcpy(y_image, bmpAttack, bmpAttackSize);
            }
            else
            {
                yImageSize = bmpToCollideSize;
                memcpy(y_image, bmpToCollide, bmpToCollideSize);
            }
            f_function(x_image, xImageSize, x, hashByteLength);
            f_function(y_image, yImageSize, y, hashByteLength);
        }

        //hash de x et ecrit dans une nouvelle version de l'image
        md5_small(x, hashByteLength, hashBitSize, x_image, xImageSize);

        //hash de y
        md5_small(y, hashByteLength, hashBitSize, y_image, yImageSize);
        if(switchImage(y))
        {
            yImageSize = bmpAttackSize;
            memcpy(y_image, bmpAttack, yImageSize);
        }
        else
        {
            yImageSize = bmpToCollideSize;
            memcpy(y_image, bmpToCollide, bmpToCollideSize);
        }
        f_function(y_image, yImageSize, y, hashByteLength);
        md5_small(y, hashByteLength, hashBitSize, y_image, yImageSize);

        //Display
        if(i != 0 && i%100 == 0)
            ReleasePrintf("%u tours effectués...\n", i);
        DebugPrintf("Tour %u: x = %s y = %s\n", 
            i + 1, 
                    PrintHexaUnsignedCharArray(x, hashByteLength, hexaHashStr),
                    PrintHexaUnsignedCharArray(y, hashByteLength, hexaHashStr));

        ++i;
    }
    while(memcmp(x, y, hashByteLength * sizeof(unsigned char)) != 0);

    unsigned int i1 = i;
    
    printf("Début de la passe 2.\n");

    if(switchImage(x))
    {
        yImageSize = bmpAttackSize;
        memcpy(y_image, bmpAttack, yImageSize);
    }
    else
    {
        yImageSize = bmpToCollideSize;
        memcpy(y_image, bmpToCollide, yImageSize);
    }
    f_function(y_image, yImageSize, x, hashByteLength);

    xImageSize = bmpAttackSize;
    memcpy(x_image, bmpAttack, xImageSize);

    i = 0;
    do
    {
        if(i != 0)
        {
            if(switchImage(x))
            {
                xImageSize = bmpAttackSize;
                memcpy(x_image, bmpAttack, bmpAttackSize);
            }
            else
            {
                xImageSize = bmpToCollideSize;
                memcpy(x_image, bmpToCollide, bmpToCollideSize);
            }
            if(switchImage(y))
            {
                yImageSize = bmpAttackSize;
                memcpy(y_image, bmpAttack, bmpAttackSize);
            }
            else
            {
                yImageSize = bmpToCollideSize;
                memcpy(y_image, bmpToCollide, bmpToCollideSize);
            }
            f_function(x_image, xImageSize, x, hashByteLength);
            f_function(y_image, yImageSize, y, hashByteLength);
        }

        //hash de x
        md5_small(x, hashByteLength, hashBitSize, x_image, xImageSize);

        //hash de y
        md5_small(y, hashByteLength, hashBitSize, y_image, yImageSize);

        //Display
        if(i != 0 && i%100 == 0)
            ReleasePrintf("%u tours effectués...\n", i);
        DebugPrintf("Tour %u: x = %s y = %s\n", 
            i + 1, 
                    PrintHexaUnsignedCharArray(x, hashByteLength, hexaHashStr),
                    PrintHexaUnsignedCharArray(y, hashByteLength, hexaHashStr));

        ++i;
    }
    while(memcmp(x, y, hashByteLength * sizeof(unsigned char)) != 0);
    
    unsigned int i2 = i;

    clock_t endTimer = clock();
    double execTime = (double)(startTimer = endTimer) / CLOCKS_PER_SEC;

    printf("Nombre de tours pour la passe 1 : %u (2^%u)\nNombre de tours pour la passe 2 : %u (2^%u)\nTemps d'exécution : %f seconde(s)\n", i1, GetPow2(i1), i2, GetPow2(i2), execTime);
    printf("Collision trouvée avec la valeur de clé égale à : %s\n", PrintHexaUnsignedCharArray(x,hashByteLength, hexaHashStr));

    //Clean des allocations
    free(bmpAttack);
    free(bmpToCollide);
    if(hexaHashStr != NULL)
        free(hexaHashStr);

    return 0;
}